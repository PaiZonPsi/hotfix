﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Video;

public class VideoPlayerScript : MonoBehaviour
{
    VideoPlayer videoPlayer;

    [SerializeField] VideoClip _secondClip;
    [SerializeField] int _MainMenuBuildIndex = 5;

    private int index = 0;
    private float startTime;
    private float bufforTime = 4f;

    // Start is called before the first frame update
    void Start()
    {
        videoPlayer = gameObject.GetComponent<VideoPlayer>();
        videoPlayer.Play();
        Debug.Log("FirstPlay");
        startTime = Time.time;
    }

    // Update is called once per frame
    void Update()
    {
        if (!videoPlayer.isPlaying && Time.time - startTime > bufforTime)
        {
            if(index == 0)
            {
                Debug.Log("Load Second clip");
                videoPlayer.clip = _secondClip;
                videoPlayer.Play();
                startTime = Time.time;
                index = 1;
            }
            else
            {
                Debug.Log("Change Scene");
                SceneManager.LoadScene(_MainMenuBuildIndex, LoadSceneMode.Single);
            }
        }
    }
}
