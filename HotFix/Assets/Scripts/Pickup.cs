﻿
using UnityEngine;

[CreateAssetMenu(fileName = "New pickup", menuName = "Pickup")]
public class Pickup : ScriptableObject
{
    public Sprite Artwork;
    public int Points;
    public float RotationSpeed;
}
