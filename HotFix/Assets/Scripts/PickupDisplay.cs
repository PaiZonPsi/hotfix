﻿using UnityEngine;

public class PickupDisplay : MonoBehaviour
{
    [SerializeField]
    private Pickup pickup;
    public int Points { get => pickup.Points; }
    public float RotationSpeed { get => pickup.RotationSpeed; }

    void Start()
    {
        SpriteRenderer spriteRenderer = gameObject.GetComponent<SpriteRenderer>();
        spriteRenderer.sprite = pickup.Artwork;
    }
}
