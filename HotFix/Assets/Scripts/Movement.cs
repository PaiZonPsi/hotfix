using System;
using System.Collections;
using System.Collections.Generic;
using System.Numerics;
using UnityEditorInternal;
using UnityEngine;
using Quaternion = UnityEngine.Quaternion;
using Random = System.Random;
using Vector2 = UnityEngine.Vector2;
using Vector3 = UnityEngine.Vector3;

[RequireComponent(typeof(Rigidbody2D))] [RequireComponent(typeof(BoxCollider2D))]
public class Movement : MonoBehaviour 
{
    [SerializeField] private float _speed = 2.0f;
    [Header("Jump settings")]
    [SerializeField] private float _jumpForce = 2.0f;
    [SerializeField] private float _groundCheckRadius = 2.0f;
    [SerializeField] private float _groundCheckDistance = 5.0f;
    [SerializeField] private bool _canMultipleJump = false;
    [SerializeField] private int _maxJumps = 2;
    
    private ParticleSystem _jumpParticles = null;
    private Rigidbody2D _rb = null;
    private Animator _animator = null;
    private float _horizontal = 0.0f;
    private bool _jump = false;
    private int _jumpCount = 0;


    private const float HorizontalAnimationPrecision = 0.2f;
    private static readonly int Horizontal = Animator.StringToHash("Horizontal");
    private static readonly int JumpAnimatorTrigger = Animator.StringToHash("Jump");
    
    private const string KGroundLayer = "Floor";
    private const float KGroundCheckOffset = .5f;
    public bool FacingRight { get; private set; } = true;

    private void Awake()
    {
        _rb = GetComponent<Rigidbody2D>();
        _jumpParticles = GetComponentInChildren<ParticleSystem>();
        _animator = GetComponent<Animator>();
    }

    private void Update()
    {
        _horizontal = Input.GetAxis("Horizontal");

        if (_horizontal > -HorizontalAnimationPrecision && _horizontal < HorizontalAnimationPrecision)
        {
            _animator.SetFloat(Horizontal, 0.0f);
        }else
        {
            _animator.SetFloat(Horizontal, 1.0f);
        }
        if (Input.GetKeyDown(KeyCode.Space))
        {
            //StartCoroutine(WaitFor());
            _jump = true;
        }

        // if (IsGrounded())
        // {
        //     if (Input.GetKeyDown(KeyCode.Space) && OnRightWall())
        //     {
        //         _rb.velocity = Vector2.up * _jumpForce;
        //         _rb.velocity = Vector2.left * _speed;
        //     }else if (Input.GetKeyDown(KeyCode.Space) && OnLeftWall())
        //     {
        //         //_rb.velocity = Vector2.up * _jumpForce;
        //         //_rb.velocity = Vector2.right * _speed;
        //         _rb.AddForce(new Vector2(_speed, _jumpForce), ForceMode2D.Force);
        //     }
        // }

        /*if (Input.GetKeyDown(KeyCode.Space) && IsGrounded())
        {
            bool wallHit = false;
            int wallHitDirection = 0;
            if (_horizontal != 0)
            {
                if (OnLeftWall())
                {
                    wallHit = true;
                    wallHitDirection = 1;
                }
                else if (OnRightWall())
                {
                    wallHit = true;
                    wallHitDirection = -1;
                }
            }

            if (!wallHit)
            {
                if (IsGrounded())
                {
                    _rb.velocity = new Vector2(_rb.velocity.x, _jumpForce);
                }
                else
                {
                    _rb.velocity = new Vector2(_jumpForce * wallHitDirection, _jumpForce);
                }
            }
        }*/
        
    }

    private void FixedUpdate()
    {
        _rb.velocity = new Vector2(_horizontal * _speed, _rb.velocity.y);

        if (IsGrounded() && _jump)
        {
            _jumpCount = 0;
            Jump();
        } else if (_jump && !IsGrounded() && _canMultipleJump && _jumpCount < _maxJumps)
        {
            Jump();
        }
        
        if ((_horizontal > 0 && !FacingRight) || (_horizontal < 0 && FacingRight)) 
        { 
            Flip();
        }
        _jump = false;
        if (_animator.GetBool(JumpAnimatorTrigger) && IsGrounded()) 
        {
            _animator.SetBool(JumpAnimatorTrigger, false);
        }
    }

    private bool IsGrounded()
    {
        return Physics2D.CircleCast(new Vector2(transform.position.x, transform.position.y - KGroundCheckOffset), _groundCheckRadius,transform.up, _groundCheckDistance, LayerMask.GetMask(KGroundLayer));
    }

    private void Jump()
    {
        _animator.SetBool("Jump 0", true); 

        StartCoroutine(WaitFor());
        _jumpCount++;
        _rb.velocity = Vector2.up * _jumpForce;
    }

    private void Flip()
    {
        FacingRight = !FacingRight;
        var rotate = transform.rotation;
        rotate = Quaternion.identity;
        rotate.y = FacingRight ? 0 : 180;
        transform.rotation = rotate;
    }

    private IEnumerator WaitFor()
    {
        yield return new WaitUntil(() => IsGrounded() == true);
        _animator.SetBool("Jump 0", false); 

    }

    public void PlayStepSound()
    {
        AudioPlayer.Instance.Play("Step");
    }
}
