﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerInteractionHandler : MonoBehaviour
{
    [SerializeField]
    private ScoreHolder scoreHolder;

    [SerializeField]
    private int _trapPenaltyScore = -5;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void OnTriggerEnter2D(Collider2D collider2D)
    {
        string layer = LayerMask.LayerToName(collider2D.gameObject.layer);
        switch (layer)
        {
            case "Pickup":
                HandlePickupInteraction(collider2D);
                break;
            case "Trap":
                HandleTrapInteraction(collider2D);
                break;
        }
    }

    void HandlePickupInteraction(Collider2D collider2D)
    {
        PickupAnimationController pickupAnimationController = collider2D.gameObject.GetComponent<PickupAnimationController>();
        PickupDisplay pickupDisplay = collider2D.gameObject.GetComponent<PickupDisplay>();
        if (pickupAnimationController == null || pickupDisplay == null)
            return;

        scoreHolder?.NotifyAboutPickupOrTrapInteraction(pickupDisplay.Points > 0, ScoreHolder.InteractionKind.Pickup);
        pickupAnimationController.OnFaceHit += OnFaceHitObserver;
        pickupAnimationController.RunCollectedAnimation();
    }

    void HandleTrapInteraction(Collider2D collider2D)
    {
        scoreHolder?.NotifyAboutPickupOrTrapInteraction(false, ScoreHolder.InteractionKind.Trap);
        int points = Mathf.Clamp(_trapPenaltyScore, -20, -1);
        scoreHolder?.ChangeScore(points, points > 0);
    }

    private void OnFaceHitObserver(object sender, PickupAnimationController.FaceHitEventArgs e)
    {
        scoreHolder?.ChangeScore(e.Points, e.IsPositive);
    }
}
