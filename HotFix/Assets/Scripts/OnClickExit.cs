﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OnClickExit : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        gameObject.GetComponent<Button>().onClick.AddListener(OnClickExitTask);
    }

    void OnClickExitTask()
    {
        Debug.Log("Exit");
        Application.Quit();
    }

}
