﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class OnClickScript : MonoBehaviour
{
    [SerializeField] private int _nextSceneBuildIndex = -1;

    // Start is called before the first frame update
    void Start()
    {
        gameObject.GetComponent<Button>().onClick.AddListener(OnClickStartGame);
    }

    void OnClickStartGame()
    {
        if(_nextSceneBuildIndex < 0)
        {
            Debug.LogError($"{gameObject.name} does not have next scene index defined");
            return;
        }
        SceneManager.LoadScene(_nextSceneBuildIndex, LoadSceneMode.Single);
    }
}
