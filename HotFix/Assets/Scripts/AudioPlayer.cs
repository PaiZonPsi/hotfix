﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = System.Random;

public class AudioPlayer : MonoBehaviour
{
    [SerializeField] private Audio[] _audios = Array.Empty<Audio>();
    private static AudioPlayer _instance = null;
    public static AudioPlayer Instance => _instance;

    private void Awake()
    {
        if (_instance == null)
        {
            _instance = this;
        }
        else
        {            
            Destroy(gameObject);
        }

    }

    private void Initialize(Audio audio)
    {
        //int rand = UnityEngine.Random.Range(0, audio.clips.Count - 1);
        audio.source.clip = audio.clip;
        audio.source.loop = audio.loop;
        audio.source.pitch = UnityEngine.Random.Range(audio._minPitch, audio._maxPitch);
        audio.source.playOnAwake = audio.playOnAwake;
        audio.source.loop = audio.loop;
    }

    public void Play(string audioName)
    {
        foreach (Audio audio in _audios)
        {
            if (audio.name == audioName)
            {
                Initialize(audio);
                audio.source.Play();
            }
        }
    }
}


[System.Serializable]
public class Audio
{
    public string name = String.Empty;
    public AudioClip clip = null;
    public AudioSource source = null;
    [Range(0.2f, 1.0f)]public float volume = 1.0f;
    [Range(-3.0f, 3.0f)]public float _maxPitch = 1.0f;
    [Range(-3.0f, 3.0f)]public float _minPitch = -1.0f;
    public bool playOnAwake = false;
    public bool loop = false;
}
