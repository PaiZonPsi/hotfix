﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PowrotDoMenuButtonClick : MonoBehaviour
{
    [SerializeField] private int _MainMenuBuildIndex = 5;
    // Start is called before the first frame update
    void Start()
    {
        gameObject.GetComponent<Button>().onClick.AddListener(OnClickChangeScene);
    }

    void OnClickChangeScene()
    {
        SceneManager.LoadScene(_MainMenuBuildIndex, LoadSceneMode.Single);
    }

}
