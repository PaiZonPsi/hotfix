﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ScoreHolder : MonoBehaviour
{
    [SerializeField] private float _maxScore = 100;
    [SerializeField] private float _minScore = 0;
    [SerializeField] private int _winSceneBuildIndex = 3;
    [SerializeField] private int _loseSceneBuildIndex = 2;
    
    private float _currentScore = 50;

    public EventHandler<ScoreChangedEventArgs> ScoreChanged;
    public EventHandler<PickupOrTrapInteractionEventArgs> OnPickupAndTrapInteraction;

    public void ChangeScore(float points, bool isReward)
    {
        if (points == 0)
            return;

        if (isReward)
        {
            AddPoints(points);
        }
        else
        {
            TakeAwayPoints(points);
        }

        ScoreChanged?.Invoke(this, new ScoreChangedEventArgs(_currentScore));

        if(_currentScore >= _maxScore)
        {
            DeclareVictory();
        }
        else if (_currentScore <= _minScore)
        {
            DeclareFailure();
        }
    }

    private void AddPoints(float prizePoints)
    {
        _currentScore += prizePoints;
    }

    private void TakeAwayPoints(float penaltyPoints)
    {
        _currentScore += penaltyPoints;
    }

    private void DeclareVictory()
    {
        SceneManager.LoadScene(_winSceneBuildIndex, LoadSceneMode.Single);
    }

    private void DeclareFailure()
    {
        SceneManager.LoadScene(_loseSceneBuildIndex, LoadSceneMode.Single);
    }

    public void NotifyAboutPickupOrTrapInteraction(bool isPositive, InteractionKind interactionKind)
    {
        OnPickupAndTrapInteraction?.Invoke(this, new PickupOrTrapInteractionEventArgs(isPositive, interactionKind));
    }

    public enum InteractionKind
    {
        Pickup,
        Trap
    }

    public class PickupOrTrapInteractionEventArgs : EventArgs
    {
        public PickupOrTrapInteractionEventArgs(bool isPositive, InteractionKind interactionKind)
        {
            IsPositive = isPositive;
            InteractionKind = interactionKind;
        }

        public bool IsPositive { get; set; }
        public InteractionKind InteractionKind { get; set; }
    }

    public class ScoreChangedEventArgs : EventArgs
    {
        public ScoreChangedEventArgs(float score)
        {
            Score = score;
        }

        public float Score { get; set; }
    }
}
