﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening.Core;
using DG.Tweening.Core.Enums;
using DG.Tweening.Plugins;
using DG.Tweening.Plugins.Core.PathCore;
using DG.Tweening.Plugins.Options;
using DG.Tweening;
using System;

public class PickupAnimationController : MonoBehaviour
{
    [SerializeField]
    public Camera cam;

    public EventHandler<FaceHitEventArgs> OnFaceHit { get; set; }

    private PickupDisplay _pickupDisplay;
    private bool IsCollectAnimation;
    private float flyTime = 0.05f;
    private Vector2[] flyPath;
    private int index = 0;
    private float timeStart = 0;
    private float timeStop = 0;


    void Start()
    {
        cam = Camera.main;
        _pickupDisplay = gameObject.GetComponent<PickupDisplay>();
    }

    // Update is called once per frame
    void Update()
    {
        transform.Rotate(Vector3.up, Map(_pickupDisplay.RotationSpeed, -100, 100, -2 * Mathf.PI, 2 * Mathf.PI));

        if (IsCollectAnimation)
        {
            transform.Rotate(Vector3.left, Map(_pickupDisplay.RotationSpeed, -100, 100, -2 * Mathf.PI, 2 * Mathf.PI));
            int endIndex = flyPath.Length - 1;
            if (index >= endIndex)
            {
                HandleFaceHit(_pickupDisplay.Points);
                Destroy(gameObject);
            }
            else
            {

                if (timeStart == 0 && timeStop == 0)
                {
                    timeStart = Time.time;
                    timeStop = timeStart + flyTime;
                }

                float distanceFraction = Map(Time.time, timeStart, timeStop, 0, 1);
                
                Vector2 start = cam.ScreenToWorldPoint(flyPath[index]);
                Vector2 stop = cam.ScreenToWorldPoint(flyPath[index + 1]);

                //gameObject.transform.position = Vector2.Lerp(start, stop, distanceFraction);
                gameObject.transform.position = new Vector3(
                    Map(distanceFraction, 0, 1, start.x, stop.x),
                    Map(distanceFraction, 0, 1, start.y, stop.y),
                    -1f
                    );
                if (distanceFraction >= 1)
                {
                    index++;
                    timeStart = Time.time;
                    timeStop = timeStart + flyTime;
                }
            }
        }
    }

    float Map(float value, float valMin, float valMax, float outMin, float outMax)
    {
        return (value - valMin) / (valMax - valMin) * (outMax - outMin) + outMin;
    }

    public void RunCollectedAnimation()
    {
        if (IsCollectAnimation)
            return;

        Vector2 start = gameObject.transform.position;
        float cameraH = 2f * cam.orthographicSize;
        float cameraW = cameraH * cam.aspect;
        Vector2 stop = new Vector2( cam.transform.position.x + cameraW / 2, cam.transform.position.y - cam.orthographicSize);
        Vector2 startCamSpc = cam.WorldToScreenPoint(start);
        Vector2 stopCamSpc = (Vector2)cam.WorldToScreenPoint(stop) + Vector2.up * 50 + Vector2.left * 50;

        float dist = stopCamSpc.x - startCamSpc.x;

        Vector2 apexCamSpc = new Vector2(startCamSpc.x + dist / 3, startCamSpc.y + dist/5);

        if(startCamSpc.x - apexCamSpc.x != 0)
        {
            double a = (stopCamSpc.y - apexCamSpc.y) / ((stopCamSpc.x - apexCamSpc.x) * (stopCamSpc.x - apexCamSpc.x));
            double deltaX = 20;
            int pointNumber = (int)((stopCamSpc.x - startCamSpc.x) / deltaX);
            pointNumber = Mathf.Clamp(pointNumber, 2, int.MaxValue);
            flyPath = new Vector2[pointNumber];
            flyPath[0] = startCamSpc;
            for(int i = 1; i < flyPath.Length - 1; i++)
            {
                double x = startCamSpc.x + deltaX * i;
                flyPath[i] = new Vector2((float)x, CalcParabola(a, x, apexCamSpc.x, apexCamSpc.y));
            }
            flyPath[flyPath.Length - 1] = stopCamSpc;

        }
        else
        {
            flyPath = new Vector2[] { startCamSpc, apexCamSpc, stopCamSpc };
        }
        IsCollectAnimation = true;
    }

    private float CalcParabola(double a, double x, double p, double q)
    {
        return (float)((a * (x - p) * (x - p)) + q);
    }

    private void HandleFaceHit(int points)
    {
        bool isPositive = points > 0;
        OnFaceHit?.Invoke(this, new FaceHitEventArgs(points, isPositive));
    }

    

    public class FaceHitEventArgs : EventArgs
    {
        public FaceHitEventArgs(int points, bool isPositive)
        {
            Points = points;
            IsPositive = isPositive;
        }

        public int Points { get; set; }
        public bool IsPositive { get; set; }
    }
}
