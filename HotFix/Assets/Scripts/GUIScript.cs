﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using System.Linq;

public class GUIScript : MonoBehaviour
{
    private Canvas canvas;
    [SerializeField] private TextMeshProUGUI scoreText;
    [SerializeField] private ScoreHolder scoreHolder;
    [SerializeField] private Image faceImage;
    [SerializeField] private TextMeshProUGUI balloonText;
    [SerializeField] private Sprite[] faces;
    [SerializeField] private Sprite angryFace;
    [SerializeField] private Sprite lovelyFace;
    [SerializeField] private float _maxScore = 100f;
    [SerializeField] private float _minScore = 0f;
    [SerializeField] private float reactionTime = 0.25f;

    private int _LevelScore = 50;

    private float reactionStartTime = -1;

    string[] _positiveTexts;
    string[] _negativeTexts;
    List<string> _currentPositiveTexts = new List<string>();
    List<string> _currentNegativeTexts = new List<string>();


    public enum FaceReactions
    {
        None,
        Lovely,
        Pissed
    }


    // Start is called before the first frame update
    void Start()
    {
        canvas = GetComponent<Canvas>();
        scoreHolder.ScoreChanged += ScoreChangedObserver;
        scoreHolder.OnPickupAndTrapInteraction += PickupOrTrapObserver;
        ChangeFaceStatus();
        scoreText.text = _LevelScore.ToString() + "%";

        loadTexts();
    }

    void loadTexts()
    {
        _positiveTexts = System.IO.File.ReadAllLines(@"Assets/Resources/PositiveTexts.txt");
        _negativeTexts = System.IO.File.ReadAllLines(@"Assets/Resources/NegativeTexts.txt");
        _currentPositiveTexts.AddRange(_positiveTexts);
        _currentNegativeTexts.AddRange(_negativeTexts);
    }

    // Update is called once per frame
    void Update()
    {
        float now = Time.time;
        if(reactionStartTime > 0)
        {
            if(now - reactionStartTime > reactionTime)
            {
                ChangeFaceStatus();

                reactionStartTime = -1f;
            }
        }
    }

    void ChangeFaceStatus()
    {
        int numberOfFaces = faces.Length;
        float range = _maxScore - _minScore;
        float increment = range / (float)numberOfFaces;

        int index = (int)(_LevelScore / increment);
        index = Mathf.Clamp(index, 0, numberOfFaces - 1);

        faceImage.sprite = faces[index];
        //faceImage.Rebuild(CanvasUpdate.PreRender);

        //for (int i = 0; i <= 5; i++)
        //{
        //    if ((_LevelScore >= i * 20) && (_LevelScore <= (i+1)*20))
        //        _actFace = i;
        //}
        //Sprite mySprite = Resources.Load<Sprite>("face" + _actFace.ToString());
        //faceImage.sprite = mySprite;
    }
    
    void ScoreChangedObserver(object sender, ScoreHolder.ScoreChangedEventArgs e)
    {
        try
        {
            ReactToScoreChanged((e.Score > _LevelScore)? FaceReactions.Lovely : FaceReactions.Pissed);
        }
        catch
        {

        }
        _LevelScore = (int)e.Score;
        scoreText.text = _LevelScore.ToString() + "%";
    }

    void ReactToScoreChanged(FaceReactions faceReaction)
    {
        switch (faceReaction) {
            case FaceReactions.Lovely:
                faceImage.sprite = lovelyFace;
                break;
            case FaceReactions.Pissed:
                faceImage.sprite = angryFace;
                break;
        }
        //faceImage.Rebuild(CanvasUpdate.PreRender);
        reactionStartTime = Time.time;
    }

    void PickupOrTrapObserver(object sender, ScoreHolder.PickupOrTrapInteractionEventArgs e)
    {
        try
        {
            ChangeBalloonText(e.IsPositive);
        }
        catch
        {

        }
    }

    void RandomText(List<string> currentList, string[] list)
    {
        System.Random rnd = new System.Random();
        int len = currentList.Count - 1;
        int r = rnd.Next(len);
        balloonText.text = currentList?[r];
        currentList.RemoveAt(r);
        if (currentList.Count == 0)
            currentList.AddRange(list);

    }

    void ChangeBalloonText(bool positive)
    {
        if (positive)
        {
            RandomText(_currentPositiveTexts, _positiveTexts);
        }
        else 
        {
            RandomText(_currentNegativeTexts, _negativeTexts);
        }


    }
}
