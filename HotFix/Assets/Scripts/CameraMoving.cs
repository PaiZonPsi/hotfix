﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMoving : MonoBehaviour
{
    [SerializeField] private float _distFromBorder = 0f;
    [SerializeField] private float _actOffset = 0f;
    [SerializeField] private float _cameraFloatingSpeed = 1f;
    [SerializeField] private float _hudHeight = 2f;

    private float _cameraW, _cameraH;
    private float _cameraOffsetW, _cameraOffsetH;
    private Camera cam;
    public GameObject player;
    private bool _cameraIsFloatingX = false;
    private bool _cameraIsFloatingY = false;
    private bool _falling = false;

    private Movement movement;

    // Start is called before the first frame update
    void Start()
    {
        cam = GetComponent<Camera>();
        cam.transform.Translate(0f, -_hudHeight, 0f);
        //float player_W = GetComponent<SpriteRenderer>().bounds.size.x;
        _cameraH = 2f * cam.orthographicSize;
        _cameraW = _cameraH * cam.aspect;
        _cameraOffsetW = _cameraW /*- player_W*/ - _distFromBorder;
        _cameraOffsetH = _cameraH - _distFromBorder;
        movement = player.GetComponent<Movement>();
    }

    // Update is called once per frame
    void Update()
    {
        setCamera(ref cam, player.transform.position.x, player.transform.position.y);
    }

    void setCamera(ref Camera cam, float playerX, float playerY)
    {
        if (_cameraIsFloatingX)
        {
            _actOffset += _cameraFloatingSpeed;
            if (movement.FacingRight)
                cam.transform.Translate(_cameraFloatingSpeed, 0f, 0f);
            else
                cam.transform.Translate(-_cameraFloatingSpeed, 0f, 0f);
            if (_actOffset > _cameraOffsetW)
            {
                _actOffset = 0f;
                _cameraIsFloatingX = false;
            }
        }
        else if (_cameraIsFloatingY)
        {
            _actOffset += _cameraFloatingSpeed;
            if (_falling)
                cam.transform.Translate(0f, -_cameraFloatingSpeed, 0f, 0f);
            else
                cam.transform.Translate(0f, _cameraFloatingSpeed, 0f, 0f);

            if (_falling)
            {
                if (_actOffset > _cameraOffsetH - _hudHeight)
                {
                    _actOffset = 0f;
                    _cameraIsFloatingY = false;
                    cam.transform.Translate(0f, -_hudHeight, 0f);
                }
            }
            else
            {
                if (_actOffset > _cameraOffsetH)

                {
                    _actOffset = 0f;
                    _cameraIsFloatingY = false;
                    //cam.transform.Translate(0f, -_hudHeight, 0f);
                }
            }

        }
        else
        {
            if ((movement.FacingRight) && (playerX - cam.transform.position.x + _cameraW / 2 >= _cameraW - _distFromBorder))
            {
                _cameraIsFloatingX = true;
            }
            else if ((!movement.FacingRight) && (playerX - _distFromBorder < cam.transform.position.x - _cameraW / 2))
            {
                _cameraIsFloatingX = true;
            }
            if (playerY - cam.transform.position.y + _cameraH / 2 >= _cameraH - _distFromBorder)
            {
                _cameraIsFloatingY = true;
                _falling = false;                
            }
            else if (playerY - _distFromBorder < cam.transform.position.y - _cameraH / 2)
            {
                _cameraIsFloatingY = true;
                _falling = true;
            }         
            //else
              //  cam.transform.Translate(0f, -_hudHeight, 0f);

        }
    }
}
